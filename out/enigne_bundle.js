/*
 * File Name: Time.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/
class Time {
    #m_DeltaTime;
    #m_FrameStartTime;
    #m_FrameEndTime;
    frameStart() {
        this.#m_FrameStartTime = Date.now();
    }
    frameEnd() {
        this.#m_FrameEndTime = Date.now();
    }
    getDelta() {
        return this.#m_DeltaTime = (this.#m_FrameStartTime - this.#m_FrameEndTime) / 1000;
    }
}
const time = new Time();
/*
 * File Name: Keyboard.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-02)
 * Contributers:
*/
var KeyState;
(function (KeyState) {
    KeyState[KeyState["keyDown"] = 0] = "keyDown";
    KeyState[KeyState["keyUp"] = 1] = "keyUp";
})(KeyState || (KeyState = {}));
var Keys;
(function (Keys) {
    Keys[Keys["backspace"] = 8] = "backspace";
    Keys[Keys["tab"] = 9] = "tab";
    Keys[Keys["enter"] = 13] = "enter";
    Keys[Keys["shift"] = 16] = "shift";
    Keys[Keys["control"] = 17] = "control";
    Keys[Keys["alt"] = 18] = "alt";
    Keys[Keys["pause_break"] = 19] = "pause_break";
    Keys[Keys["caps_lock"] = 20] = "caps_lock";
    Keys[Keys["escape"] = 27] = "escape";
    Keys[Keys["space"] = 32] = "space";
    Keys[Keys["pageUp"] = 33] = "pageUp";
    Keys[Keys["pageDown"] = 34] = "pageDown";
    Keys[Keys["end"] = 35] = "end";
    Keys[Keys["home"] = 36] = "home";
    Keys[Keys["left_arrow"] = 37] = "left_arrow";
    Keys[Keys["up_arrow"] = 38] = "up_arrow";
    Keys[Keys["right_arrow"] = 39] = "right_arrow";
    Keys[Keys["down_arrow"] = 40] = "down_arrow";
    Keys[Keys["print_screen"] = 44] = "print_screen";
    Keys[Keys["insert"] = 45] = "insert";
    Keys[Keys["delete"] = 46] = "delete";
    Keys[Keys["a_0"] = 48] = "a_0";
    Keys[Keys["a_1"] = 49] = "a_1";
    Keys[Keys["a_2"] = 50] = "a_2";
    Keys[Keys["a_3"] = 51] = "a_3";
    Keys[Keys["a_4"] = 52] = "a_4";
    Keys[Keys["a_5"] = 53] = "a_5";
    Keys[Keys["a_6"] = 54] = "a_6";
    Keys[Keys["a_7"] = 55] = "a_7";
    Keys[Keys["a_8"] = 56] = "a_8";
    Keys[Keys["a_9"] = 57] = "a_9";
    Keys[Keys["a"] = 65] = "a";
    Keys[Keys["b"] = 66] = "b";
    Keys[Keys["c"] = 67] = "c";
    Keys[Keys["d"] = 68] = "d";
    Keys[Keys["e"] = 69] = "e";
    Keys[Keys["f"] = 70] = "f";
    Keys[Keys["g"] = 71] = "g";
    Keys[Keys["h"] = 72] = "h";
    Keys[Keys["i"] = 73] = "i";
    Keys[Keys["j"] = 74] = "j";
    Keys[Keys["k"] = 75] = "k";
    Keys[Keys["l"] = 76] = "l";
    Keys[Keys["m"] = 77] = "m";
    Keys[Keys["n"] = 78] = "n";
    Keys[Keys["o"] = 79] = "o";
    Keys[Keys["p"] = 80] = "p";
    Keys[Keys["q"] = 81] = "q";
    Keys[Keys["r"] = 82] = "r";
    Keys[Keys["s"] = 83] = "s";
    Keys[Keys["t"] = 84] = "t";
    Keys[Keys["u"] = 85] = "u";
    Keys[Keys["v"] = 86] = "v";
    Keys[Keys["w"] = 87] = "w";
    Keys[Keys["x"] = 88] = "x";
    Keys[Keys["y"] = 89] = "y";
    Keys[Keys["z"] = 90] = "z";
    Keys[Keys["left_super"] = 91] = "left_super";
    Keys[Keys["right_super"] = 92] = "right_super";
    Keys[Keys["select_key"] = 93] = "select_key";
    Keys[Keys["numpad0"] = 96] = "numpad0";
    Keys[Keys["numpad1"] = 97] = "numpad1";
    Keys[Keys["numpad2"] = 98] = "numpad2";
    Keys[Keys["numpad3"] = 99] = "numpad3";
    Keys[Keys["numpad4"] = 100] = "numpad4";
    Keys[Keys["numpad5"] = 101] = "numpad5";
    Keys[Keys["numpad6"] = 102] = "numpad6";
    Keys[Keys["numpad7"] = 103] = "numpad7";
    Keys[Keys["numpad8"] = 104] = "numpad8";
    Keys[Keys["numpad9"] = 105] = "numpad9";
    Keys[Keys["nummultiply"] = 106] = "nummultiply";
    Keys[Keys["numadd"] = 107] = "numadd";
    Keys[Keys["numsubtract"] = 109] = "numsubtract";
    Keys[Keys["numfull_stop"] = 110] = "numfull_stop";
    Keys[Keys["numdivide"] = 111] = "numdivide";
    Keys[Keys["f1"] = 112] = "f1";
    Keys[Keys["f2"] = 113] = "f2";
    Keys[Keys["f3"] = 114] = "f3";
    Keys[Keys["f4"] = 115] = "f4";
    Keys[Keys["f5"] = 116] = "f5";
    Keys[Keys["f6"] = 117] = "f6";
    Keys[Keys["f7"] = 118] = "f7";
    Keys[Keys["f8"] = 119] = "f8";
    Keys[Keys["f9"] = 120] = "f9";
    Keys[Keys["f10"] = 121] = "f10";
    Keys[Keys["f11"] = 122] = "f11";
    Keys[Keys["f12"] = 123] = "f12";
    Keys[Keys["num_lock"] = 144] = "num_lock";
    Keys[Keys["scroll_lock"] = 145] = "scroll_lock";
    Keys[Keys["semi_colon"] = 186] = "semi_colon";
    Keys[Keys["equals"] = 187] = "equals";
    Keys[Keys["comma"] = 188] = "comma";
    Keys[Keys["dash"] = 189] = "dash";
    Keys[Keys["full_stop"] = 190] = "full_stop";
    Keys[Keys["forward_slash"] = 191] = "forward_slash";
    Keys[Keys["backtick"] = 192] = "backtick";
    Keys[Keys["open_bracket"] = 219] = "open_bracket";
    Keys[Keys["back_slash"] = 220] = "back_slash";
    Keys[Keys["close_bracket"] = 221] = "close_bracket";
    Keys[Keys["single_quote"] = 222] = "single_quote";
})(Keys || (Keys = {}));
class Keyboard {
    m_Keys = new Array(222);
    constructor() {
        this.m_Keys.forEach(key => {
            key = KeyState.keyUp;
        });
    }
    isKeyDown(key) {
        return (this.m_Keys[key] == KeyState.keyDown) ? true : false;
    }
    isKeyUp(key) {
        return (this.m_Keys[key] == KeyState.keyUp) ? true : false;
    }
}
const keyboard = new Keyboard();
document.addEventListener("keydown", e => {
    keyboard.m_Keys[e.keyCode] = KeyState.keyDown;
});
document.addEventListener("keyup", e => {
    keyboard.m_Keys[e.keyCode] = KeyState.keyUp;
});
/*
 * File Name: Mouse.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-02)
 * Contributers:
*/
var MouseState;
(function (MouseState) {
    MouseState[MouseState["buttonDown"] = 0] = "buttonDown";
    MouseState[MouseState["buttonUp"] = 1] = "buttonUp";
})(MouseState || (MouseState = {}));
var Buttons;
(function (Buttons) {
    Buttons[Buttons["left"] = 0] = "left";
    Buttons[Buttons["middle"] = 1] = "middle";
    Buttons[Buttons["right"] = 2] = "right";
    Buttons[Buttons["back"] = 3] = "back";
    Buttons[Buttons["forward"] = 4] = "forward";
})(Buttons || (Buttons = {}));
class Mouse {
    x = 0;
    y = 0;
    m_Buttons = new Array(5);
    constructor() {
        this.m_Buttons.forEach(button => {
            button = MouseState.buttonUp;
        });
    }
    isButtonDown(button) {
        return (this.m_Buttons[button] == MouseState.buttonDown) ? true : false;
    }
    isButtonUp(button) {
        return (this.m_Buttons[button] == MouseState.buttonUp) ? true : false;
    }
    getMousePos() {
        return new Vec2(this.x, this.y);
    }
}
const mouse = new Mouse();
addEventListener("mousedown", e => {
    mouse.m_Buttons[e.button] = MouseState.buttonDown;
});
addEventListener("mouseup", e => {
    mouse.m_Buttons[e.button] = MouseState.buttonUp;
});
addEventListener("mousemove", e => {
    mouse.x = e.clientX;
    mouse.y = e.clientY;
});
/*
 * File Name: Vec2.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/
class Vec2 {
    x = 0;
    y = 0;
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    Add(rhs) {
        return new Vec2(this.x + rhs.x, this.y + rhs.y);
    }
    Subtract(rhs) {
        return new Vec2(this.x - rhs.x, this.y - rhs.y);
    }
    Multiply(rhs) {
        return new Vec2(this.x * rhs.x, this.y * rhs.y);
    }
    Divide(rhs) {
        return new Vec2(this.x / rhs.x, this.y / rhs.y);
    }
    Scale(scale) {
        return new Vec2(this.x * scale, this.y * scale);
    }
}
/*
 * File Name: Entity.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/
class Entity {
    m_ID;
    m_Tag;
    m_Enabled = true;
    m_ShouldBeDestroyed = false;
    constructor(id, tag) {
        this.m_ID = id;
        this.m_Tag = tag;
    }
    m_Transform = null;
}
/*
 * File Name: EntityManager.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/
class EntityManager {
    #m_EntityCount = 0;
    #m_Entities = [];
    #m_KeepBuffer = [];
    AddEntity(tag) {
        let e = new Entity(this.#m_EntityCount++, tag);
        this.#m_Entities.push(e);
        return e;
    }
    Update() {
        // Destroy entities
        this.#m_Entities.forEach(e => {
            if (!e.m_ShouldBeDestroyed) {
                this.#m_KeepBuffer.push(e);
            }
        });
        this.#m_Entities = [];
        this.#m_Entities = this.#m_KeepBuffer;
        this.#m_KeepBuffer = [];
        this.#m_Entities.forEach(e => {
            if (e.m_Enabled) {
                e.m_Transform?.System();
            }
        });
    }
}
const entityManager = new EntityManager();
/*
 * File Name: Transform.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/
class cTransform {
    m_Position;
    m_LastPosition;
    m_Size;
    m_Velocity;
    constructor(pos, size, vel) {
        this.m_Position = pos;
        this.m_Size = size;
        this.m_Velocity = vel;
    }
    System() {
        this.m_LastPosition = this.m_Position;
        this.m_Position.Add(this.m_Velocity);
    }
}
