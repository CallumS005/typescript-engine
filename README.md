# TS Engine

Welcome to the TS engine, this is a 2D game engine for creating web games, this came around for a team project that required us to make a game within a web browser. Before we started to plan and develop the game I decided to create the start of an engine for personal use, giving permission to use the code of the engine for the project under the [GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007](./LICENCE.md).

## Contributers

Callum (@CallumS005) - Project Creator