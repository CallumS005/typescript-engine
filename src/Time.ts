/*
 * File Name: Time.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/


class Time {
	#m_DeltaTime: number;
	#m_FrameStartTime: number;
	#m_FrameEndTime: number;

	frameStart() {
		this.#m_FrameStartTime = Date.now();
	}

	frameEnd() {
		this.#m_FrameEndTime = Date.now();	
	}

	getDelta(): number {
		return this.#m_DeltaTime = (this.#m_FrameStartTime - this.#m_FrameEndTime) / 1000;
	}
}

const time = new Time();