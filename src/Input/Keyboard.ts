/*
 * File Name: Keyboard.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-02)
 * Contributers:
*/


enum KeyState {
	keyDown,
	keyUp
}

enum Keys {
	backspace = 8,
	tab = 9,
	enter = 13,
	shift = 16,
	control,
	alt,
	pause_break,
	caps_lock,
	escape = 27,
	space = 32,
	pageUp,
	pageDown,
	end,
	home,
	left_arrow,
	up_arrow,
	right_arrow,
	down_arrow,
	print_screen = 44,
	insert,
	delete,
	a_0 = 48,
	a_1,
	a_2,
	a_3,
	a_4,
	a_5,
	a_6,
	a_7,
	a_8,
	a_9,
	a = 65,
	b,
	c,
	d,
	e,
	f,
	g,
	h,
	i,
	j,
	k,
	l,
	m,
	n,
	o,
	p,
	q,
	r,
	s,
	t,
	u,
	v,
	w,
	x,
	y,
	z,
	left_super,
	right_super,
	select_key,
	numpad0 = 96,
	numpad1, 
	numpad2,
	numpad3,
	numpad4,
	numpad5,
	numpad6,
	numpad7,
	numpad8,
	numpad9,
	nummultiply,
	numadd,
	numsubtract = 109,
	numfull_stop,
	numdivide,
	f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12,
	num_lock = 144,
	scroll_lock,
	semi_colon = 186,
	equals,
	comma,
	dash,
	full_stop,
	forward_slash,
	backtick,
	open_bracket = 219,
	back_slash,
	close_bracket,
	single_quote 
}

class Keyboard {
	m_Keys: KeyState[] = new Array<KeyState>(222);

	constructor() {
		this.m_Keys.forEach(key => {
			key = KeyState.keyUp;
		});
	}

	isKeyDown(key: Keys): boolean {
		return (this.m_Keys[key] == KeyState.keyDown) ? true : false;
	}

	isKeyUp(key: Keys): boolean {
		return (this.m_Keys[key] == KeyState.keyUp) ? true : false;
	}
}

const keyboard = new Keyboard();

document.addEventListener("keydown", e => {
	keyboard.m_Keys[e.keyCode] = KeyState.keyDown;
});

document.addEventListener("keyup", e => {
	keyboard.m_Keys[e.keyCode] = KeyState.keyUp;
});
