/*
 * File Name: Mouse.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-02)
 * Contributers:
*/


enum MouseState {
	buttonDown,
	buttonUp
}

enum Buttons {
	left = 0,
	middle,
	right,
	back,
	forward
}

class Mouse {
	x: number = 0;
	y: number = 0;
	m_Buttons: MouseState[] = new Array<MouseState>(5);

	constructor() {
		this.m_Buttons.forEach(button => {
			button = MouseState.buttonUp;
		});
	}

	isButtonDown(button: Buttons): boolean {
		return (this.m_Buttons[button] == MouseState.buttonDown) ? true : false;
	}

	isButtonUp(button: Buttons): boolean {
		return (this.m_Buttons[button] == MouseState.buttonUp) ? true : false;
	}

	getMousePos(): Vec2 {
		return new Vec2(this.x, this.y);
	}
}

const mouse = new Mouse();

addEventListener("mousedown", e => {
	mouse.m_Buttons[e.button] = MouseState.buttonDown;
});

addEventListener("mouseup", e => {
	mouse.m_Buttons[e.button] = MouseState.buttonUp;
});

addEventListener("mousemove", e => {
	mouse.x = e.clientX;
	mouse.y = e.clientY;
});