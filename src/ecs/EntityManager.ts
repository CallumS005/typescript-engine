/*
 * File Name: EntityManager.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/


class EntityManager {
	#m_EntityCount: number = 0;
	#m_Entities: Entity[] = [];

	#m_KeepBuffer: Entity[] = [];

	AddEntity(tag: string): Entity {
		let e = new Entity(this.#m_EntityCount++, tag);
		this.#m_Entities.push(e);
		return e;
	}

	Update() {
		// Destroy entities
		this.#m_Entities.forEach(e => {
			if (!e.m_ShouldBeDestroyed) {
				this.#m_KeepBuffer.push(e);
			}
		});

		this.#m_Entities = [];
		this.#m_Entities = this.#m_KeepBuffer;
		this.#m_KeepBuffer = [];

		this.#m_Entities.forEach(e => {
			if (e.m_Enabled) {
				e.m_Transform?.System();
			}
		});
	}
}

const entityManager = new EntityManager();