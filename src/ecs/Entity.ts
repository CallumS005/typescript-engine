/*
 * File Name: Entity.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/


class Entity {
	m_ID: number;
	m_Tag: string;
	m_Enabled: boolean = true;
	m_ShouldBeDestroyed: boolean = false;

	constructor(id: number, tag: string) {
		this.m_ID = id;
		this.m_Tag = tag;
	}

	m_Transform: cTransform = null;
}