/*
 * File Name: Transform.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/


class cTransform {
	m_Position: Vec2;
	m_LastPosition: Vec2;
	m_Size: Vec2;
	m_Velocity: Vec2;

	constructor(pos: Vec2, size: Vec2, vel: Vec2) {
		this.m_Position = pos;
		this.m_Size = size;
		this.m_Velocity = vel;
	}
	
	System() {
		this.m_LastPosition = this.m_Position;
		this.m_Position.Add(this.m_Velocity);
	}
}