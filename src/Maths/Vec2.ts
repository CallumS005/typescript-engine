/*
 * File Name: Vec2.ts
 * Created By: @CallumS005
 * Date/Time: (2023-03-01)
 * Contributers:
*/


class Vec2 {
	x: number = 0;
	y: number = 0;

	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}

	Add(rhs: Vec2): Vec2 {
		return new Vec2(this.x + rhs.x, this.y + rhs.y);
	}

	Subtract(rhs: Vec2): Vec2 {
		return new Vec2(this.x - rhs.x, this.y - rhs.y);
	}

	Multiply(rhs: Vec2): Vec2 {
		return new Vec2(this.x * rhs.x, this.y * rhs.y);
	}

	Divide(rhs: Vec2): Vec2 {
		return new Vec2(this.x / rhs.x, this.y / rhs.y);
	}

	Scale(scale: number): Vec2 {
		return new Vec2(this.x * scale, this.y * scale);
	}
}